package ru.tsc.babeshko.tm.repository;

import ru.tsc.babeshko.tm.api.repository.IUserRepository;
import ru.tsc.babeshko.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeById(String id) {
        final User user = findById(id);
        users.remove(user);
        return user;
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        users.remove(user);
        return user;
    }

    @Override
    public User remove(User user) {
        users.remove(user);
        return user;
    }

}