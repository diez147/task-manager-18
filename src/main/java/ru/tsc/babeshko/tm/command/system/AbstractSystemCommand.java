package ru.tsc.babeshko.tm.command.system;

import ru.tsc.babeshko.tm.api.service.ICommandService;
import ru.tsc.babeshko.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
