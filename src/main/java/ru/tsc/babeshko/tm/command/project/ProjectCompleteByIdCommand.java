package ru.tsc.babeshko.tm.command.project;

import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "proj-complete-by-id";

    public static final String DESCRIPTION = "Complete project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.COMPLETED);
    }

}