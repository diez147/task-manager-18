package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByEmail(String email);

    User findByLogin(String login);

    User removeById(String id);

    User removeByLogin(String login);

    User remove(User user);

}